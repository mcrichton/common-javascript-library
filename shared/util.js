// Copyright (c) 2019 NCTech Imaging Ltd. All rights reserved.

class LogUtil {
	static tag (tag) {
		const expandedTag = tag.padStart(10, " ");
		return [`[${LogUtil.getFormattedTimestamp()}]`, `[${expandedTag}]`];
	}

	static getFormattedTimestamp (epoch) {
		const d = epoch ? new Date(epoch) : new Date();
		return `${d.getFullYear()}-${LogUtil._getTimestamp_getPadded(d.getMonth() + 1)}-${LogUtil._getTimestamp_getPadded(d.getDate())} ${LogUtil._getTimestamp_getPadded(d.getHours())}:${LogUtil._getTimestamp_getPadded(d.getMinutes())}:${LogUtil._getTimestamp_getPadded(d.getSeconds())}`;
	}

	static _getTimestamp_getPadded (num) {
		return `${num}`.padStart(2, "0");
	}
}

class SortUtil {
	static ascSort (a, b) {
		if (b === a) return 0;
		return b < a ? 1 : -1;
	}

	static ascSortLower (a, b) {
		a = a ? a.toLowerCase() : a;
		b = b ? b.toLowerCase() : b;
		return SortUtil.ascSort(a, b)
	}
}

class MiscUtil {
	static cpy (obj) {
		return JSON.parse(JSON.stringify(obj));
	}

	/** Get by path. **/
	static get (object, ...path) {
		if (object == null) return null;
		for (let i = 0; i < path.length; ++i) {
			object = object[path[i]];
			if (object == null) return object;
		}
		return object;
	}

	/** Set by path. **/
	static set (object, ...pathAndVal) {
		if (object == null) return null;

		const val = pathAndVal.pop();
		if (!pathAndVal.length) return null;

		const len = pathAndVal.length;
		for (let i = 0; i < len; ++i) {
			const pathPart = pathAndVal[i];
			if (i === len - 1) object[pathPart] = val;
			else object = (object[pathPart] = object[pathPart] || {});
		}

		return val;
	}

	/** Delete by path. */
	static delete (object, ...path) {
		if (object == null) return;
		for (let i = 0; i < path.length - 1; ++i) {
			object = object[path[i]];
			if (object === undefined) return;
		}
		if (object) delete object[path.last()];
	}

	/** Deep equals. */
	static eq (a, b) {
		if (MiscUtil._eq_sameValueZeroEqual(a, b)) return true;
		if (a && b && typeof a === "object" && typeof b === "object") {
			if (MiscUtil._eq_isPlainObject(a) && MiscUtil._eq_isPlainObject(b)) return MiscUtil._eq_areObjectsEqual(a, b);
			const arrayA = Array.isArray(a);
			const arrayB = Array.isArray(b);
			if (arrayA || arrayB) return arrayA === arrayB && MiscUtil._eq_areArraysEqual(a, b);
			const setA = a instanceof Set;
			const setB = b instanceof Set;
			if (setA || setB) return setA === setB && MiscUtil._eq_areSetsEqual(a, b);
			return MiscUtil._eq_areObjectsEqual(a, b);
		}
		return false;
	}

	// This is intentionally checking for NaN !== NaN, so ignore the linter's complaints
	// eslint-disable-next-line no-self-compare
	static _eq_sameValueZeroEqual (a, b) {
		return a === b || (a !== a && b !== b);
	}

	static _eq_isPlainObject (value) {
		return value.constructor === Object || value.constructor == null;
	}

	static _eq_areObjectsEqual (a, b) {
		const keysA = Object.keys(a);
		const {length} = keysA;
		if (Object.keys(b).length !== length) return false;
		for (let i = 0; i < length; i++) {
			if (!Object.prototype.hasOwnProperty.call(b, keysA[i])) return false;
			if (!MiscUtil.eq(a[keysA[i]], b[keysA[i]])) return false;
		}
		return true;
	}

	static _eq_areArraysEqual (a, b) {
		const {length} = a;
		if (b.length !== length) return false;
		for (let i = 0; i < length; i++) if (!MiscUtil.eq(a[i], b[i])) return false;
		return true;
	}

	static _eq_areSetsEqual (a, b) {
		if (a.size !== b.size) return false;
		for (const it of a) if (!b.has(it)) return false;
		return true;
	}

	static key (obj, val) {
		for (const k in obj) if (Object.prototype.hasOwnProperty.call(obj, k) && obj[k] === val) return k;
		return null;
	}

	static pDelay (msecs) {
		return new Promise(resolve => setTimeout(resolve, msecs));
	}

	static pDelayError (msecs, error) {
		return new Promise((resolve, reject) => setTimeout(reject.bind(null, error), msecs));
	}

	/**
	 * @param [opts]
	 * @param [opts.keyBlacklist]
	 * @param [opts.isAllowDeleteObjects] If returning `undefined` from an object handler should be treated as a delete.
	 * @param [opts.isAllowDeleteArrays] (Unimplemented) // TODO
	 * @param [opts.isAllowDeleteBooleans] (Unimplemented) // TODO
	 * @param [opts.isAllowDeleteNumbers] (Unimplemented) // TODO
	 * @param [opts.isAllowDeleteStrings] (Unimplemented) // TODO
	 * @param [opts.isDepthFirst] If array/object recursion should occur before array/object primitive handling.
	 * @param [opts.isNoModification] If the walker should not attempt to modify the data.
	 */
	static getWalker (opts) {
		opts = opts || {};
		const keyBlacklist = opts.keyBlacklist || new Set();

		function applyHandlers (handlers, obj, lastKey, stack) {
			if (!(handlers instanceof Array)) handlers = [handlers];
			handlers.forEach(h => {
				const out = h(obj, lastKey, stack);
				if (!opts.isNoModification) obj = out;
			});
			return obj;
		}

		function runHandlers (handlers, obj, lastKey, stack) {
			if (!(handlers instanceof Array)) handlers = [handlers];
			handlers.forEach(h => h(obj, lastKey, stack));
		}

		const fn = (obj, primitiveHandlers, lastKey, stack) => {
			if (obj == null) {
				if (primitiveHandlers.null) return applyHandlers(primitiveHandlers.null, obj, lastKey, stack);
				return obj;
			}

			const doObjectRecurse = () => {
				Object.keys(obj).forEach(k => {
					const v = obj[k];
					if (!keyBlacklist.has(k)) {
						const out = fn(v, primitiveHandlers, k, stack);
						if (!opts.isNoModification) obj[k] = out;
					}
				});
			};

			const to = typeof obj;
			switch (to) {
				case undefined:
					if (primitiveHandlers.preUndefined) runHandlers(primitiveHandlers.preUndefined, obj, lastKey, stack);
					if (primitiveHandlers.undefined) {
						const out = applyHandlers(primitiveHandlers.undefined, obj, lastKey, stack);
						if (!opts.isNoModification) obj = out;
					}
					if (primitiveHandlers.postUndefined) runHandlers(primitiveHandlers.postUndefined, obj, lastKey, stack);
					return obj;
				case "boolean":
					if (primitiveHandlers.preBoolean) runHandlers(primitiveHandlers.preBoolean, obj, lastKey, stack);
					if (primitiveHandlers.boolean) {
						const out = applyHandlers(primitiveHandlers.boolean, obj, lastKey, stack);
						if (!opts.isNoModification) obj = out;
					}
					if (primitiveHandlers.postBoolean) runHandlers(primitiveHandlers.postBoolean, obj, lastKey, stack);
					return obj;
				case "number":
					if (primitiveHandlers.preNumber) runHandlers(primitiveHandlers.preNumber, obj, lastKey, stack);
					if (primitiveHandlers.number) {
						const out = applyHandlers(primitiveHandlers.number, obj, lastKey, stack);
						if (!opts.isNoModification) obj = out;
					}
					if (primitiveHandlers.postNumber) runHandlers(primitiveHandlers.postNumber, obj, lastKey, stack);
					return obj;
				case "string":
					if (primitiveHandlers.preString) runHandlers(primitiveHandlers.preString, obj, lastKey, stack);
					if (primitiveHandlers.string) {
						const out = applyHandlers(primitiveHandlers.string, obj, lastKey, stack);
						if (!opts.isNoModification) obj = out;
					}
					if (primitiveHandlers.postString) runHandlers(primitiveHandlers.postString, obj, lastKey, stack);
					return obj;
				case "object": {
					if (obj instanceof Array) {
						if (primitiveHandlers.preArray) runHandlers(primitiveHandlers.preArray, obj, lastKey, stack);
						if (opts.isDepthFirst) {
							if (stack) stack.push(obj);
							const out = obj.map(it => fn(it, primitiveHandlers, lastKey, stack));
							if (!opts.isNoModification) obj = out;
							if (stack) stack.pop();

							if (primitiveHandlers.array) {
								const out = applyHandlers(primitiveHandlers.array, obj, lastKey, stack);
								if (!opts.isNoModification) obj = out;
							}
						} else {
							if (primitiveHandlers.array) {
								const out = applyHandlers(primitiveHandlers.array, obj, lastKey, stack);
								if (!opts.isNoModification) obj = out;
							}
							const out = obj.map(it => fn(it, primitiveHandlers, lastKey, stack));
							if (!opts.isNoModification) obj = out;
						}
						if (primitiveHandlers.postArray) runHandlers(primitiveHandlers.postArray, obj, lastKey, stack);
						return obj;
					} else {
						if (primitiveHandlers.preObject) runHandlers(primitiveHandlers.preObject, obj, lastKey, stack);
						if (opts.isDepthFirst) {
							if (stack) stack.push(obj);
							doObjectRecurse();
							if (stack) stack.pop();

							if (primitiveHandlers.object) {
								const out = applyHandlers(primitiveHandlers.object, obj, lastKey, stack);
								if (!opts.isNoModification) obj = out;
							}
							if (obj == null) {
								if (!opts.isAllowDeleteObjects) throw new Error(`Object handler(s) returned null!`);
							}
						} else {
							if (primitiveHandlers.object) {
								const out = applyHandlers(primitiveHandlers.object, obj, lastKey, stack);
								if (!opts.isNoModification) obj = out;
							}
							if (obj == null) {
								if (!opts.isAllowDeleteObjects) throw new Error(`Object handler(s) returned null!`);
							} else {
								doObjectRecurse();
							}
						}
						if (primitiveHandlers.postObject) runHandlers(primitiveHandlers.postObject, obj, lastKey, stack);
						return obj;
					}
				}
				default: throw new Error(`Unhandled type "${to}"`);
			}
		};

		return {walk: fn};
	}
}

Array.prototype.equals = Array.prototype.equals || function (array2) {
	const array1 = this;
	if (!array1 && !array2) return true;
	else if ((!array1 && array2) || (array1 && !array2)) return false;

	let temp = [];
	if ((!array1[0]) || (!array2[0])) return false;
	if (array1.length !== array2.length) return false;
	let key;
	// Put all the elements from array1 into a "tagged" array
	for (let i = 0; i < array1.length; i++) {
		key = (typeof array1[i]) + "~" + array1[i]; // Use "typeof" so a number 1 isn't equal to a string "1".
		if (temp[key]) temp[key]++;
		else temp[key] = 1;
	}
	// Go through array2 - if same tag missing in "tagged" array, not equal
	for (let i = 0; i < array2.length; i++) {
		key = (typeof array2[i]) + "~" + array2[i];
		if (temp[key]) {
			if (temp[key] === 0) return false;
			else temp[key]--;
		} else return false;
	}
	return true;
};

Array.prototype.last = Array.prototype.last || function (arg) {
	if (arg !== undefined) this[this.length - 1] = arg;
	else return this[this.length - 1];
};

/** Map each array item to a k:v pair, then flatten them into one object. */
Array.prototype.mergeMap = Array.prototype.mergeMap || function (fnMap) {
	return this.map((...args) => fnMap(...args)).reduce((a, b) => Object.assign(a, b), {});
};

Array.prototype.nextWrap = Array.prototype.nextWrap || function (item) {
	const ix = this.indexOf(item);
	if (~ix) {
		if (ix + 1 < this.length) return this[ix + 1];
		else return this[0];
	} else return this.last();
};

Array.prototype.prevWrap = Array.prototype.prevWrap || function (item) {
	const ix = this.indexOf(item);
	if (~ix) {
		if (ix - 1 >= 0) return this[ix - 1];
		else return this.last();
	} else return this[0];
};

String.prototype.uppercaseFirst = String.prototype.uppercaseFirst || function () {
	const str = this.toString();
	if (str.length <= 1) return str.toUpperCase();
	return `${str[0].toUpperCase()}${str.slice(1)}`;
};

String.prototype.qq = String.prototype.qq || function () {
	return this.replace(/'/g, `&apos;`).replace(/"/g, `&quot;`);
};

String.prototype.uq = String.prototype.uq || function () {
	return this.replace(/&apos;/g, `'`).replace(/&quot;/g, `"`);
};

String.prototype.toTitleCase = String.prototype.toTitleCase || function () {
	let str = this.replace(/([^\W_]+[^\s-/]*) */g, m0 => m0.charAt(0).toUpperCase() + m0.substr(1).toLowerCase());

	// Require space surrounded, as title-case requires a full word on either side
	String.prototype.toTitleCase._TITLE_LOWER_WORDS_RE = String.prototype.toTitleCase._TITLE_LOWER_WORDS_RE = String.prototype.toTitleCase._TITLE_LOWER_WORDS.map(it => new RegExp(`\\s${it}\\s`, "gi"));
	String.prototype.toTitleCase._TITLE_UPPER_WORDS_RE = String.prototype.toTitleCase._TITLE_UPPER_WORDS_RE = String.prototype.toTitleCase._TITLE_UPPER_WORDS.map(it => new RegExp(`\\b${it}\\b`, "g"));

	const len = String.prototype.toTitleCase._TITLE_LOWER_WORDS.length;
	for (let i = 0; i < len; i++) {
		str = str.replace(
			String.prototype.toTitleCase._TITLE_LOWER_WORDS_RE[i],
			txt => txt.toLowerCase()
		);
	}

	const len1 = String.prototype.toTitleCase._TITLE_UPPER_WORDS.length;
	for (let i = 0; i < len1; i++) {
		str = str.replace(
			String.prototype.toTitleCase._TITLE_UPPER_WORDS_RE[i],
			String.prototype.toTitleCase._TITLE_UPPER_WORDS[i].toUpperCase()
		);
	}

	return str;
};
String.prototype.toTitleCase._TITLE_LOWER_WORDS = ["a", "an", "the", "and", "but", "or", "for", "nor", "as", "at", "by", "for", "from", "in", "into", "near", "of", "on", "onto", "to", "with"];
	// Certain words such as initialisms or acronyms should be left uppercase
String.prototype.toTitleCase._TITLE_UPPER_WORDS = ["Id", "Tv", "Dm", "Ok"];

export {LogUtil, SortUtil, MiscUtil};
