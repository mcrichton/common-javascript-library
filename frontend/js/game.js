class Snake {
	constructor () {
		this._cvn = null;
		this._ctx = null;
		this._cvnBuf = null;
		this._ctxBuf = null;

		this._state = null;
		this._cellsSnake = [];

		this._tLastMove = null;

		this._lastDirection = null;
		this._direction = null;
		this._score = null;
		this._isPaused = null;
		this._tDead = null;
		this._resetVars();

		this._$parent = null;
	}

	render ($parent) {
		this._$parent = $parent
			.empty()
			.css({position: "relative"})
			.append(`<div class="muted italic mb-2 text-center">SPACE to begin</div>`);

		const cvsSize = Snake._PX_CELL * Snake._BOARD_SIZE;

		this._cnv = document.createElement("canvas");
		this._cnv.width = this._cnv.height = cvsSize;
		this._cnv.style.width = this._cnv.style.height = `${cvsSize}px`;
		this._cnv.style.border = "1px solid black";
		this._cnv.tabIndex = 0;

		this._cvnBuf = document.createElement("canvas");
		this._cvnBuf.width = this._cvnBuf.height = cvsSize;
		this._cvnBuf.style.width = this._cnv.style.height = `${cvsSize}px`;

		this._cnv.addEventListener("keydown", evt => this._handleKey(evt));

		this._ctx = this._cnv.getContext("2d");
		this._ctxBuf = this._cvnBuf.getContext("2d");

		this._ctx.imageSmoothingEnabled = false;
		this._ctxBuf.imageSmoothingEnabled = false;

		$parent.append(this._cnv);

		this._reset();
	}

	_reset () {
		this._state = [];
		for (let x = 0; x < Snake._BOARD_SIZE; ++x) {
			for (let y = 0; y < Snake._BOARD_SIZE; ++y) {
				(this._state[x] = this._state[x] || [])[y] = new Snake._Cell(x, y);
			}
		}

		const cellSnake = this._state[Math.floor(Snake._BOARD_SIZE / 2)][Math.floor(Snake._BOARD_SIZE / 2)];
		cellSnake.isSnake = true;
		this._cellsSnake = [cellSnake];

		this._doSpawnApple();

		this._tLastMove = Date.now();

		this._resetVars();

		window.requestAnimationFrame(this._tick.bind(this));
	}

	_resetVars () {
		this._lastDirection = null;
		this._direction = Snake._DIR_RIGHT;
		this._score = 0;
		this._isPaused = true;
		this._tDead = null;
	}

	_doSpawnApple () {
		const emptyCells = this._state.flat().filter(it => !it.isSnake);
		const cell = emptyCells[Math.round(emptyCells.length * Math.random())];
		cell.isApple = true;
	}

	_tick () {
		if (this._isPaused) {
			this._renderFrame();
			setTimeout(() => {
				this._tLastMove = Date.now();
				this._tick();
			}, 750);
			return;
		}

		const now = Date.now();
		if (!this._tDead) {
			const delta = now - this._tLastMove;

			const moves = Math.floor(delta / Snake._MOVE_TIME);
			for (let i = 0; i < moves; ++i) this._doMove();

			this._tLastMove = this._tLastMove + (moves * Snake._MOVE_TIME);
		} else {
			const delta = now - this._tDead;
			if (delta >= 1000) {
				const $btnOk = $(`<button class="btn btn-default btn-sm">OK</button>`)
					.click(() => {
						$wrpLose.remove();
						this._cnv.focus();
					});

				const $wrpLose = $$`<div class="flex-vh-center" style="position: absolute; top: 0; right: 0; bottom: 0; left: 0; background: #ffffff; border: 1px solid black;">
					<div class="flex-col">
						<div class="text-center mb-2">You lose! Your final score was: ${this._score}</div>
						${$btnOk}
					</div>
				</div>`;
				this._$parent.append($wrpLose);
				$btnOk.focus();

				this._reset();
			}
		}

		this._renderFrame();

		window.requestAnimationFrame(this._tick.bind(this));
	}

	_doMove () {
		const headCell = this._cellsSnake[0];
		const tailCell = this._cellsSnake.slice(-1)[0];

		let nxtCell;
		switch (this._direction) {
			case Snake._DIR_RIGHT: nxtCell = this._state[headCell.x + 1]?.[headCell.y]; break;
			case Snake._DIR_DOWN: nxtCell = this._state[headCell.x]?.[headCell.y + 1]; break;
			case Snake._DIR_LEFT: nxtCell = this._state[headCell.x - 1]?.[headCell.y]; break;
			case Snake._DIR_UP: nxtCell = this._state[headCell.x]?.[headCell.y - 1]; break;
		}
		this._lastDirection = this._direction;

		if (!nxtCell || nxtCell.isSnake) {
			this._tDead = Date.now();
		} else if (nxtCell.isApple) {
			nxtCell.isApple = false;
			nxtCell.isSnake = true;
			this._cellsSnake.unshift(nxtCell);
			this._score++;
			this._doSpawnApple();
		} else {
			nxtCell.isSnake = true;
			tailCell.isSnake = false;
			this._cellsSnake.unshift(nxtCell);
			this._cellsSnake.pop();
		}
	}

	_renderFrame () {
		this._ctxBuf.clearRect(0, 0, this._cvnBuf.width, this._cvnBuf.height);

		for (let x = 0; x < Snake._BOARD_SIZE; ++x) {
			for (let y = 0; y < Snake._BOARD_SIZE; ++y) {
				this._state[x][y].render(this._ctxBuf, {isDead: !!this._tDead});
			}
		}

		this._ctx.drawImage(this._cvnBuf, 0, 0);
	}

	_handleKey (evt) {
		switch (evt.key) {
			case "ArrowRight": evt.stopPropagation(); evt.preventDefault(); if (this._lastDirection !== Snake._DIR_LEFT) this._direction = Snake._DIR_RIGHT; break;
			case "ArrowDown": evt.stopPropagation(); evt.preventDefault(); if (this._lastDirection !== Snake._DIR_UP) this._direction = Snake._DIR_DOWN; break;
			case "ArrowLeft": evt.stopPropagation(); evt.preventDefault(); if (this._lastDirection !== Snake._DIR_RIGHT) this._direction = Snake._DIR_LEFT; break;
			case "ArrowUp": evt.stopPropagation(); evt.preventDefault(); if (this._lastDirection !== Snake._DIR_DOWN) this._direction = Snake._DIR_UP; break;
			case " ": evt.stopPropagation(); evt.preventDefault(); this._isPaused = !this._isPaused; break;
		}
	}
}
Snake._BOARD_SIZE = 40;
Snake._PX_CELL = 10;
Snake._MOVE_TIME = 150;
Snake._DIR_RIGHT = 0;
Snake._DIR_DOWN = 1;
Snake._DIR_LEFT = 2;
Snake._DIR_UP = 4;

Snake._Cell = class {
	constructor (x, y) {
		this.x = x;
		this.y = y;
		this.isSnake = false;
		this.isApple = false;
	}

	render (ctx, opts) {
		if (this.isSnake) ctx.fillStyle = opts?.isDead ? "#d48500" : "#5ea100";
		else if (this.isApple) ctx.fillStyle = "#ff0000";
		else ctx.fillStyle = "#ffffff";

		ctx.fillRect(
			this.x * Snake._PX_CELL,
			this.y * Snake._PX_CELL,
			(this.x + 1) * Snake._PX_CELL,
			(this.y + 1) * Snake._PX_CELL,
		);
	}
}

export {Snake};
