// Copyright (c) 2019 NCTech Imaging Ltd. All rights reserved.

import {LogUtil, MiscUtil} from "../../shared/util.js";

class CommonConst {}
CommonConst.STR_SEE_CONSOLE = `See the console (CTRL+SHIFT+J) for details.`;

class DataUtil {
	static _getCacheBustingUrl (url) {
		return `${url}${url.split("/").last().includes("?") ? "&" : "?"}t=${Date.now()}`;
	}

	static async pGetText (url) { return (await fetch(DataUtil._getCacheBustingUrl(url))).text(); }

	static async pGetJson (url) { return (await fetch(DataUtil._getCacheBustingUrl(url))).json(); }

	static async pPostJson (url, data) {
		const resp = await fetch(
			url,
			{
				method: "post",
				headers: {
					"Content-Type": "application/json",
				},
				body: JSON.stringify(data),
			},
		);
		return resp.json();
	}
}

DataUtil._LOG_TAG = "DATA";

class JqueryUtil {
	static initEnhancements () {
		window._elel_parser = new DOMParser();
		window.elel = function (parts, ...args) {
			if (parts instanceof HTMLElement) {
				return (...passed) => {
					const parts2 = [...passed[0]];
					const args2 = passed.slice(1);
					parts2[0] = `<div>${parts2[0]}`;
					parts2.last(`${parts2.last()}</div>`);

					const temp = elel(parts2, ...args2);
					temp.childNodes.forEach(n => parts.appendChild(n));
					return parts;
				};
			} else {
				const eles = [];
				let ixArg = 0;

				const handleArg = (arg) => {
					if (arg instanceof HTMLElement) {
						eles.push(arg);
						return `<${arg.tagName.toLowerCase()} data-r="true"></${arg.tagName.toLowerCase()}>`;
					} else return arg
				};

				// Allow the parts array to be modified
				parts = [...parts];

				// Ensure the absence of surrounding whitespace text nodes
				if (typeof parts[0] === "string") parts[0] = parts[0].trimStart();
				if (typeof parts.last() === "string") parts.last(parts.last().trimEnd());

				const raw = parts.reduce((html, p) => {
					const myIxArg = ixArg++;
					if (args[myIxArg] == null) return `${html}${p}`;
					if (args[myIxArg] instanceof Array) return `${html}${args[myIxArg].map(arg => handleArg(arg)).join("")}${p}`;
					else return `${html}${handleArg(args[myIxArg])}${p}`;
				});
				const res = window._elel_parser.parseFromString(raw, "text/html");

				const fragment = new DocumentFragment();

				if (res.body.childNodes.length === 1 && res.body.children[0].dataset.r === "true") {
					fragment.appendChild(res.body.children[0]);
					return fragment;
				}

				const toReplace = res.body.querySelectorAll(`[data-r=true]`);
				const len = toReplace.length;
				for (let i = 0; i < len; ++i) {
					toReplace[i].replaceWith(eles[i]);
				}

				if (res.body.childNodes.length === 1) {
					return res.body.childNodes[0];
				}

				res.body.childNodes.forEach(n => fragment.appendChild(n));

				return fragment;
			}
		}

		/**
		 * Template strings which can contain jQuery objects.
		 * Usage: $$`<div>Press this button: ${$btn}</div>`
		 * @return jQuery
		 */
		window.$$ = function (parts, ...args) {
			if (parts instanceof jQuery) {
				return (...passed) => {
					const parts2 = [...passed[0]];
					const args2 = passed.slice(1);
					parts2[0] = `<div>${parts2[0]}`;
					parts2.last(`${parts2.last()}</div>`);

					const $temp = $$(parts2, ...args2);
					$temp.children().each((i, e) => $(e).appendTo(parts));
					return parts;
				};
			} else {
				const $eles = [];
				let ixArg = 0;

				const handleArg = (arg) => {
					if (arg instanceof $) {
						$eles.push(arg);
						return `<${arg.tag()} data-r="true"></${arg.tag()}>`;
					} else if (arg instanceof HTMLElement) {
						return handleArg($(arg));
					} else return arg
				};

				const raw = parts.reduce((html, p) => {
					const myIxArg = ixArg++;
					if (args[myIxArg] == null) return `${html}${p}`;
					if (args[myIxArg] instanceof Array) return `${html}${args[myIxArg].map(arg => handleArg(arg)).join("")}${p}`;
					else return `${html}${handleArg(args[myIxArg])}${p}`;
				});
				const $res = $(raw);

				if ($res.length === 1) {
					if ($res.attr("data-r") === "true") return $eles[0];
					else $res.find(`[data-r=true]`).replaceWith(i => $eles[i]);
				} else {
					// Handle case where user has passed in a bunch of elements with no outer wrapper
					const $tmp = $(`<div></div>`);
					$tmp.append($res);
					$tmp.find(`[data-r=true]`).replaceWith(i => $eles[i]);
					return $tmp.children();
				}

				return $res;
			}
		};

		$.fn.extend({
			disableSpellcheck: function () { return this.attr("autocomplete", "new-password").attr("autocapitalize", "off").attr("spellcheck", "false"); },
			tag: function () { return this.prop("tagName").toLowerCase(); },
			title: function (...args) { return this.attr("title", ...args); },
			placeholder: function (...args) { return this.attr("placeholder", ...args); },

			hideX: function () { return this.addClass("hidden"); },
			showX: function () { return this.removeClass("hidden"); },
			toggleX: function (val) {
				if (val === undefined) return this.toggleClass("hidden", !this.hasClass("hidden"));
				else return this.toggleClass("hidden", !val);
			},

			keyupDebounced: function (fn, wait = 225) {
				const throttled = FnUtil.debounce(fn, wait, {trailing: true})
				this.on("keyup", throttled)
				return this;
			},

			fastHtml: function (html) {
				if (!this.length) return this;
				let tgt = this[0];
				while (tgt.children.length) {
					tgt = tgt.children[0];
				}
				tgt.innerHTML = html;
				return this;
			},
		});
	}
}

class UiUtil {
	static strToInt (string, fallbackEmpty = 0) {
		if (!string.trim()) return fallbackEmpty;
		const preDot = string.split(".")[0].trim();
		const unary = preDot.replace(/^([-+]*).*$/, (...m) => m[1]);
		const numPart = preDot.replace(/[^0-9]/g, "");
		const num = Number(`${unary}${numPart}` || 0);
		return isNaN(num) ? 0 : num;
	}

	/**
	 * @param [opts]
	 * @param [opts.fnClose]
	 * @param [opts.isFullWidth]
	 * @param [opts.isFullHeight]
	 * @param [opts.isMaxWidth600]
	 * @param [opts.isMaxHeight400]
	 */
	static getShowModal (opts) {
		opts = opts || {};

		if (!UiUtil._modalIsInit) {
			UiUtil._modalIsInit = true;
			$(document.body).keydown(evt => {
				if (evt.key !== "Escape") return;
				if (!UiUtil._modalStack.length) return;

				UiUtil._modalStack.last().doClose();
			});
		}

		const $body = $(`body`);
		$body.addClass("ui-modal__active");

		const doClose = (...args) => {
			const ix = UiUtil._modalStack.indexOf(out);
			if (~ix) UiUtil._modalStack.splice(ix, 1);

			if (!UiUtil._modalStack.length) $body.removeClass("ui-modal__active");
			$wrpOuter.remove();
			if (opts.fnClose) opts.fnClose(...args);
		};

		const extraClasses = [
			opts.isFullWidth ? `w-100` : "",
			opts.isFullHeight ? `h-100` : "",
			opts.isMaxWidth600 ? `ui-modal__fg--mxw-600p` : "",
			opts.isMaxHeight400 ? `ui-modal__fg--mxh-400p` : "",
		].filter(Boolean);

		const $wrpInner = $(`<div class="ui-modal__fg p-4 ${extraClasses.join(" ")}"></div>`)
			.click(evt => evt.stopPropagation());

		const $wrpOuter = $$`<div class="ui-modal__bg">${$wrpInner}</div>`
			.appendTo($body)
			.click(() => doClose());

		const out = {
			$wrpInner,
			doClose,
		};

		UiUtil._modalStack.push(out);

		return out;
	}

	static scrollIntoViewIfNeeded ($eleOrEle) {
		if ($eleOrEle instanceof jQuery) $eleOrEle = $eleOrEle[0];
		if (!$eleOrEle) return;

		// Chrome-likes only :(
		if ($eleOrEle.scrollIntoViewIfNeeded) $eleOrEle.scrollIntoViewIfNeeded();
		else $eleOrEle.scrollIntoView({block: "end"});
	}
}

UiUtil._modalIsInit = false;
UiUtil._modalStack = [];

class ComponentUtil {
	/**
	 * @param self Component.
	 * @param prop State property.
	 * @param [opts] Options object.
	 * @param [opts.ele] Element template.
	 * @param [opts.stateProp] Underlying state property to use.
	 * @returns {jQuery} Input $element.
	 */
	static $getDatePicker (self, prop, opts) {
		opts = opts || {};
		const hookProp = opts.stateProp || "state";
		const stateProp = `_${hookProp}`;
		const $iptDate = $(opts.ele || `<input type="date" class="form-control">`)
			.keydown(evt => { if (evt.key === "Escape") { evt.stopPropagation(); $iptDate.blur(); } })
			.change(() => {
				const raw = $iptDate.val().trim();
				if (raw) self[stateProp][prop] = (new Date(raw)).getTime() / 1000;
				else delete self[stateProp][prop];
			});
		const hookDate = () => {
			if (self[stateProp][prop]) $iptDate.val(ComponentUtil._getDate(self[stateProp][prop]));
			else $iptDate.val("");
		};
		self._addHook(hookProp, prop, hookDate);
		hookDate();
		return $iptDate;
	}

	/**
	 * @param self Component.
	 * @param prop State property.
	 * @param [opts] Options object.
	 * @param [opts.ele] Element template.
	 * @param [opts.stateProp] Underlying state property to use.
	 * @returns {jQuery} Input $element.
	 */
	static $getDatetimePicker (self, prop, opts) {
		opts = opts || {};
		const hookProp = opts.stateProp || "state";
		const stateProp = `_${hookProp}`;
		const $iptDatetime = $(opts.ele || `<input type="datetime-local" class="form-control">`)
			.keydown(evt => { if (evt.key === "Escape") { evt.stopPropagation(); $iptDatetime.blur(); } })
			.change(() => {
				const raw = $iptDatetime.val().trim();
				if (raw) self[stateProp][prop] = (new Date(raw)).getTime() / 1000;
				else delete self[stateProp][prop];
			});
		const hookDatetime = () => {
			if (self[stateProp][prop]) $iptDatetime.val(ComponentUtil._getDatetime(self[stateProp][prop]));
			else $iptDatetime.val("");
		};
		self._addHook(hookProp, prop, hookDatetime);
		hookDatetime();
		return $iptDatetime;
	}

	static _getDate (epochTime) {
		const d = (new Date(epochTime * 1000));
		return `${d.getFullYear()}-${`${d.getMonth() + 1}`.padStart(2, "0")}-${`${d.getDate()}`.padStart(2, "0")}`;
	}

	static _getDatetime (epochTime) {
		const d = (new Date(epochTime * 1000));
		return `${ComponentUtil._getDate(epochTime)}T${`${d.getHours()}`.padStart(2, "0")}:${`${d.getMinutes()}`.padStart(2, "0")}`;
	}

	/**
	 * @param self Component.
	 * @param prop State property.
	 * @param opts Options object.
	 * @param opts.vals Select values.
	 * @param [opts.ele] Element template.
	 * @param [opts.fnDisplay] Function which maps a val to displayable text.
	 * @param [opts.isNullable] If a null value should be displayed.
	 * @param [opts.stateProp] Underlying state property to use.
	 * @returns {jQuery} Select $element.
	 */
	static $getSelect (self, prop, opts) {
		opts = opts || {};
		const hookProp = opts.stateProp || "state";
		const stateProp = `_${hookProp}`;
		const $sel = $(`<select class="form-control"></select>`)
			.change(() => {
				const val = Number($sel.val());
				if (!~val && opts.isNullable) self[stateProp][prop] = null;
				else self[stateProp][prop] = opts.vals[val];
			});
		if (opts.isNullable) $(`<option value="-1" class="italic">${opts.fnDisplay ? opts.fnDisplay(null) : "Select..."}</option>`).appendTo($sel);
		opts.vals.forEach((v, i) => $(`<option value="${i}"></option>`).text(opts.fnDisplay ? opts.fnDisplay(v) : v).appendTo($sel));
		const hookSelect = () => {
			let ix = opts.vals.indexOf(self[stateProp][prop]);
			$sel.val(`${ix}`);
			$sel.toggleClass("italic muted", !~ix);
		};
		self._addHook(hookProp, prop, hookSelect);
		hookSelect();
		return $sel;
	}

	/**
	 * @param self Component.
	 * @param prop State property.
	 * @param [opts] Options object.
	 * @param [opts.ele] Element template.
	 * @param [opts.stateProp] Underlying state property to use.
	 * @returns {jQuery} Input $element.
	 */
	static $getInputStr (self, prop, opts) {
		opts = opts || {};
		const hookProp = opts.stateProp || "state";
		const stateProp = `_${hookProp}`;
		const $ipt = $(opts.ele || `<input class="form-control">`)
			.keydown(evt => { if (evt.key === "Escape") { evt.stopPropagation(); $ipt.blur(); } })
			.keyupDebounced(() => {
				const raw = $ipt.val().trim();
				if (raw) self[stateProp][prop] = raw;
				else delete self[stateProp][prop];
			});
		const hookIpt = () => $ipt.val(self[stateProp][prop] || "");
		self._addHook(hookProp, prop, hookIpt);
		hookIpt();
		return $ipt;
	}

	/**
	 * @param self Component.
	 * @param prop State property.
	 * @param [opts] Options object.
	 * @param [opts.ele] Element template.
	 * @param [opts.stateProp] Underlying state property to use.
	 * @returns {jQuery} Input $element.
	 */
	static $getInputText (self, prop, opts) {
		opts = opts || {};
		const hookProp = opts.stateProp || "state";
		const stateProp = `_${hookProp}`;
		const $ipt = $(opts.ele || `<textarea class="form-control resize-vertical" style="height: 70px;">`)
			.keydown(evt => { if (evt.key === "Escape") { evt.stopPropagation(); $ipt.blur(); } })
			.keyupDebounced(() => {
				const raw = $ipt.val().trim();
				if (raw) self[stateProp][prop] = raw;
				else delete self[stateProp][prop];
			});
		const hookIpt = () => $ipt.val(self[stateProp][prop] || "");
		self._addHook(hookProp, prop, hookIpt);
		hookIpt();
		return $ipt;
	}

	/**
	 * @param self Component.
	 * @param prop State property.
	 * @param [opts] Options object.
	 * @param [opts.fallbackEmpty] Fallback number if input is left empty.
	 * @param [opts.ele] Element template.
	 * @param [opts.stateProp] Underlying state property to use.
	 * @returns {jQuery} Input $element.
	 */
	static $getInputInt (self, prop, opts) {
		opts = opts || {};
		const hookProp = opts.stateProp || "state";
		const stateProp = `_${hookProp}`;
		const $ipt = $(opts.ele || `<input class="form-control text-right">`)
			.keydown(evt => { if (evt.key === "Escape") { evt.stopPropagation(); $ipt.blur(); } })
			.change(() => self[stateProp][prop] = UiUtil.strToInt($ipt.val(), opts.fallbackEmpty))
			.click(() => $ipt.select());
		const hook = () => $ipt.val(self[stateProp][prop]);
		self._addHook(hookProp, prop, hook);
		hook();
		return $ipt;
	}

	/**
	 * @param self Component.
	 * @param prop Property to hook on.
	 * @param opts Options Object.
	 * @param opts.values Values to display.
	 * @param [opts.fnDisplay] Value display function.
	 * @param [opts.isDisallowNull] True if null is not an allowed value.
	 * @param [opts.asMeta] If a meta-object should be returned containing the hook and the wrapper.
	 * @param [opts.isIndent] If the checkboxes should be indented.
	 * @param [opts.stateProp] Underlying state property to use.
	 * @return {jQuery}
	 */
	static $getCbsEnum (self, prop, opts) {
		opts = opts || {};
		const hookProp = opts.stateProp || "state";
		const stateProp = `_${hookProp}`;

		const $wrp = $(`<div class="flex-col w-100"></div>`);
		const metas = opts.values.map(it => {
			const $cb = $(`<input type="checkbox">`)
				.keydown(evt => {
					switch (evt.key) {
						case "Escape": evt.stopPropagation(); $cb.blur(); break;
						case "c":
						case "Enter": $cb.click(); break;
					}
				})
				.change(() => {
					let didUpdate = false;
					const ix = (self[stateProp][prop] || []).indexOf(it);
					if (~ix) self[stateProp][prop].splice(ix, 1);
					else {
						if (self[stateProp][prop]) self[stateProp][prop].push(it);
						else {
							didUpdate = true;
							self[stateProp][prop] = [it];
						}
					}
					if (!didUpdate) self[stateProp][prop] = [...self[stateProp][prop]];
				});

			$$`<label class="split-v-center my-1 stripe-odd ${opts.isIndent ? "ml-4" : ""}"><div class="no-wrap flex-v-center">${opts.fnDisplay ? opts.fnDisplay(it) : it}</div>${$cb}</label>`.appendTo($wrp);

			return {$cb, value: it};
		});

		const hook = () => metas.forEach(meta => meta.$cb.prop("checked", self[stateProp][prop] && self[stateProp][prop].includes(meta.value)));
		self._addHook(hookProp, prop, hook);
		hook();

		return opts.asMeta ? {$wrp, unhook: () => self._removeHook(stateProp, prop, hook), metas} : $wrp;
	}

	/**
	 * @param self Component.
	 * @param prop Property to hook on.
	 * @param [opts] Options Object.
	 * @param [opts.asMeta] If a meta-object should be returned containing the hook and the input.
	 * @param [opts.stateProp] Underlying state property to use.
	 * @return {jQuery}
	 */
	static $getCbBool (self, prop, opts) {
		opts = opts || {};
		const hookProp = opts.stateProp || "state";
		const stateProp = `_${hookProp}`;

		const $cb = $(`<input type="checkbox">`)
			.keydown(evt => {
				switch (evt.key) {
					case "Escape": evt.stopPropagation(); $cb.blur(); break;
					case "c":
					case "Enter": $cb.click(); break;
				}
			})
			.change(() => self[stateProp][prop] = $cb.prop("checked"));
		const hook = () => $cb.prop("checked", !!self[stateProp][prop]);
		self._addHook(hookProp, prop, hook);
		hook();

		return opts.asMeta ? ({$cb, unhook: () => self._removeHook(stateProp, prop, hook)}) : $cb;
	}
}

class ProxyBase {
	constructor () {
		this.__hooks = {};
		this.__hooksAll = {};
	}

	_getProxy (hookProp, toProxy) {
		return new Proxy(toProxy, {
			set: (object, prop, value) => {
				if (object[prop] === value) return true;
				object[prop] = value;
				if (this.__hooksAll[hookProp]) this.__hooksAll[hookProp].forEach(hook => hook(prop, value));
				if (this.__hooks[hookProp] && this.__hooks[hookProp][prop]) this.__hooks[hookProp][prop].forEach(hook => hook(prop, value));
				return true;
			},
			deleteProperty: (object, prop) => {
				if (!(prop in object)) return true;
				delete object[prop];
				if (this.__hooksAll[hookProp]) this.__hooksAll[hookProp].forEach(hook => hook(prop, null));
				if (this.__hooks[hookProp] && this.__hooks[hookProp][prop]) this.__hooks[hookProp][prop].forEach(hook => hook(prop, null));
				return true;
			},
		});
	}

	_addHook (hookProp, prop, hook) {
		((this.__hooks[hookProp] = this.__hooks[hookProp] || {})[prop] = (this.__hooks[hookProp][prop] || [])).push(hook);
	}

	_addHookAll (hookProp, hook) {
		(this.__hooksAll[hookProp] = this.__hooksAll[hookProp] || []).push(hook);
	}

	_removeHook (hookProp, prop, hook) {
		if (this.__hooks[hookProp] && this.__hooks[hookProp][prop]) {
			const ix = this.__hooks[hookProp][prop].findIndex(hk => hk === hook);
			if (~ix) this.__hooks[hookProp][prop].splice(ix, 1);
		}
	}

	/**
	 * Object.assign equivalent, overwrites values on the current proxied object with some new values,
	 *   then trigger all the appropriate event handlers.
	 * @param hookProp Hook property, e.g. "state". Assumes the proxy and underlying objects are
	 * stored as properties on this object with the same name, with one and two leading underscores
	 * respectively.
	 * @param toObj
	 * @param isOverwrite If the overwrite should clean/delete all data from the object beforehand.
	 */
	_proxyAssign (hookProp, toObj, isOverwrite) {
		const proxyProp = `_${hookProp}`;
		const underProp = `__${hookProp}`;

		const oldKeys = Object.keys(this[proxyProp]);
		const nuKeys = new Set(Object.keys(toObj));
		const dirtyKeys = new Set();

		if (isOverwrite) {
			oldKeys.forEach(k => {
				if (!nuKeys.has(k) && this[underProp] !== undefined) {
					delete this[underProp][k];
					dirtyKeys.add(k);
				}
			});
		}

		nuKeys.forEach(k => {
			if (!MiscUtil.eq(this[underProp][k], toObj[k])) {
				this[underProp][k] = toObj[k];
				dirtyKeys.add(k);
			}
		});

		dirtyKeys.forEach(k => {
			if (this.__hooksAll[hookProp]) this.__hooksAll[hookProp].forEach(hk => hk(k, this[underProp][k]));
			if (this.__hooks[hookProp] && this.__hooks[hookProp][k]) this.__hooks[hookProp][k].forEach(hk => hk(k, this[underProp][k]));
		});
	}
}

class ComponentBase extends ProxyBase {
	constructor () {
		super();

		// state
		this.__state = {...this._getDefaultState()};
		this._state = this._getProxy("state", this.__state);

		// data
		this.__data = {};
		this._data = this._getProxy("data", this.__data);

		// collection renders
		this.__rendered = {};

		this.__locks = {};
	}

	_addHookState (prop, hook) { this._addHook("state", prop, hook); }
	_removeHookState (prop, hook) { this._removeHook("state", prop, hook); }
	_assignState (toObj, isOverwrite) { this._proxyAssign("state", toObj, isOverwrite); }

	_addHookData (prop, hook) { this._addHook("data", prop, hook); }
	_removeHookData (prop, hook) { this._removeHook("data", prop, hook); }
	_assignData (toObj, isOverwrite) { this._proxyAssign("data", toObj, isOverwrite); }

	async _pLock (lockName) {
		while (this.__locks[lockName]) await this.__locks[lockName].lock;
		let unlock = null;
		const lock = new Promise(resolve => unlock = resolve);
		this.__locks[lockName] = {
			lock,
			unlock,
		}
	}

	_unlock (lockName) {
		const lockMeta = this.__locks[lockName];
		if (lockMeta) {
			delete this.__locks[lockName];
			lockMeta.unlock();
		}
	}

	// to be overridden as required
	_getDefaultState () {
		return {};
	}

	// region Sub-collections
	/**
	 * @param opts Options object.
	 * @param opts.prop The state property.
	 * @param [opts.namespace] The render namespace.
	 */
	_getRenderedCollection (opts) {
		opts = opts || {};
		const renderedLookupProp = opts.namespace ? `${opts.namespace}.${opts.prop}` : opts.prop;
		return (this.__rendered[renderedLookupProp] = this.__rendered[renderedLookupProp] || {});
	}

	/**
	 * @param opts Options object.
	 * @param opts.prop The state property.
	 * @param opts.fnGetNew Function to run which generates existing render meta. Arguments are `item`.
	 * @param [opts.fnDeleteExisting] Function to run on deleted render meta. Arguments are `rendered, item`.
	 * @param [opts.fnUpdateExisting] Function to run on existing render meta. Arguments are `rendered, item`.
	 * @param [opts.isDiffMode] If a diff of the state should be taken/checked before updating renders.
	 * @param [opts.namespace] A namespace to store these renders under. Useful if multiple renders are being made from
	 *        the same collection.
	 */
	_renderCollection (opts) {
		opts = opts || {};

		const rendered = this._getRenderedCollection(opts);
		const toDelete = new Set(Object.keys(rendered));

		(this._state[opts.prop] || []).forEach((it, i) => {
			if (it.id == null) throw new Error(`Collection item did not have an ID!`);
			const meta = rendered[it.id];

			toDelete.delete(it.id);
			if (meta) {
				if (opts.isDiffMode) {
					// Hashing the stringified JSON relies on the property order remaining consistent, but this is fine
					const nxtHash = CryptUtil.md5(JSON.stringify(it));
					if (nxtHash !== meta.__hash) {
						meta.__hash = nxtHash;
					} else return;
				}

				meta.data = it; // update any existing pointers
				if (opts.fnUpdateExisting) opts.fnUpdateExisting(meta, it, i);
			} else {
				const meta = opts.fnGetNew(it, i);
				meta.data = it;
				if (!meta.$wrpRow) throw new Error(`A "$wrpRow" property is required in order for deletes!`);

				if (opts.isDiffMode) meta.hash = CryptUtil.md5(JSON.stringify(it));

				rendered[it.id] = meta;
			}
		});

		this._renderCollection_doDeletes(rendered, toDelete);
	}

	_renderCollection_doDeletes (rendered, toDelete, opts) {
		opts = opts || {};

		toDelete.forEach(id => {
			const meta = rendered[id];
			meta.$wrpRow.remove();
			delete rendered[id];
			if (opts.fnDeleteExisting) opts.fnDeleteExisting(meta);
		});
	}

	_triggerCollectionUpdate (prop) {
		if (!this._state[prop]) return;
		this._state[prop] = [...this._state[prop]];
	}
	// endregion

	// region To/from object
	static fromObject (obj, ...noModCollections) {
		const comp = new this();
		Object.entries(MiscUtil.cpy(obj)).forEach(([k, v]) => {
			if (v == null) comp.__state[k] = v;
			else if (noModCollections.includes("*") || noModCollections.includes(k)) comp.__state[k] = v;
			else if (typeof v === "object" && v instanceof Array) comp.__state[k] = ComponentBase._toCollection(v);
			else comp.__state[k] = v;
		});
		return comp;
	}

	toObject () {
		const cpy = MiscUtil.cpy(this.__state);
		Object.entries(cpy).forEach(([k, v]) => {
			if (v != null && v instanceof Array && v.every(it => it && it.id)) cpy[k] = ComponentBase._fromCollection(v);
		});
		return cpy;
	}

	static _toCollection (array) { if (array) return array.map(it => ({id: CryptUtil.uid(), entity: it})); }
	static _fromCollection (array) { if (array) return array.map(it => it.entity); }
	// endregion
}

class HashStateComponentBase extends ComponentBase {
	constructor () {
		super();
		this._addHookAll("state", () => UrlUtil.saveStateHash(this._state, this._getDefaultState()));
	}

	async pInit () {
		window.addEventListener("hashchange", () => this._pHandleHashChange());
		await this._pHandleHashChange();
	}

	async _pHandleHashChange () {
		const state = UrlUtil.loadStateHash();
		Object.assign(this._state, state);
	}
}

class FnUtil {
	/**
	 * from lodash
	 * @param func
	 * @param wait
	 * @param [opts]
	 * @param [opts.leading]
	 * @param [opts.maxWait]
	 * @param [opts.trailing]
	 */
	static debounce (func, wait, opts) {
		let lastArgs;
		let lastThis;
		let maxWait;
		let result;
		let timerId;
		let lastCallTime;
		let lastInvokeTime = 0;
		let leading = false;
		let maxing = false;
		let trailing = true;

		wait = Number(wait) || 0;
		if (typeof opts === "object") {
			leading = !!opts.leading;
			maxing = "maxWait" in opts;
			maxWait = maxing ? Math.max(Number(opts.maxWait) || 0, wait) : maxWait;
			trailing = "trailing" in opts ? !!opts.trailing : trailing;
		}

		function invokeFunc (time) {
			let args = lastArgs;
			let thisArg = lastThis;

			lastArgs = lastThis = undefined;
			lastInvokeTime = time;
			result = func.apply(thisArg, args);
			return result;
		}

		function leadingEdge (time) {
			lastInvokeTime = time;
			timerId = setTimeout(timerExpired, wait);
			return leading ? invokeFunc(time) : result;
		}

		function remainingWait (time) {
			let timeSinceLastCall = time - lastCallTime;
			let timeSinceLastInvoke = time - lastInvokeTime;
			let result = wait - timeSinceLastCall;
			return maxing ? Math.min(result, maxWait - timeSinceLastInvoke) : result;
		}

		function shouldInvoke (time) {
			let timeSinceLastCall = time - lastCallTime;
			let timeSinceLastInvoke = time - lastInvokeTime;

			return (lastCallTime === undefined || (timeSinceLastCall >= wait) || (timeSinceLastCall < 0) || (maxing && timeSinceLastInvoke >= maxWait));
		}

		function timerExpired () {
			const time = Date.now();
			if (shouldInvoke(time)) {
				return trailingEdge(time);
			}
			// Restart the timer.
			timerId = setTimeout(timerExpired, remainingWait(time));
		}

		function trailingEdge (time) {
			timerId = undefined;

			if (trailing && lastArgs) return invokeFunc(time);
			lastArgs = lastThis = undefined;
			return result;
		}

		function cancel () {
			if (timerId !== undefined) clearTimeout(timerId);
			lastInvokeTime = 0;
			lastArgs = lastCallTime = lastThis = timerId = undefined;
		}

		function flush () {
			return timerId === undefined ? result : trailingEdge(Date.now());
		}

		function debounced () {
			let time = Date.now();
			let isInvoking = shouldInvoke(time);
			lastArgs = arguments;
			lastThis = this;
			lastCallTime = time;

			if (isInvoking) {
				if (timerId === undefined) return leadingEdge(lastCallTime);
				if (maxing) {
					// Handle invocations in a tight loop.
					timerId = setTimeout(timerExpired, wait);
					return invokeFunc(lastCallTime);
				}
			}
			if (timerId === undefined) timerId = setTimeout(timerExpired, wait);
			return result;
		}

		debounced.cancel = cancel;
		debounced.flush = flush;
		return debounced;
	}

	/**
	 * from lodash
	 * @param func
	 * @param wait
	 * @param [opts]
	 * @param [opts.leading]
	 * @param [opts.trailing]
	 */
	static throttle (func, wait, opts) {
		let leading = true;
		let trailing = true;

		if (typeof opts === "object") {
			leading = "leading" in opts ? !!opts.leading : leading;
			trailing = "trailing" in opts ? !!opts.trailing : trailing;
		}

		return this.debounce(func, wait, {leading, maxWait: wait, trailing});
	}
}

class UrlUtil {
	static loadStateHash () {
		const spl = UrlUtil._getHash().split(UrlUtil.HASH_SEP).map(it => it.trim()).filter(Boolean);
		if (!spl.length) return {};
		const state = {};
		spl.forEach(it => {
			const [k, v] = it.split("=");
			const decoded = decodeURIComponent(v).trim();
			state[decodeURIComponent(k)] = decoded ? JSON.parse(decoded) : null;
		});
		return state;
	}

	/**
	 * @param state The state to save to the hash.
	 * @param [defaultState] Default state. If specified, the state will be filtered to non-default
	 *   values only.
	 */
	static saveStateHash (state, defaultState) { location.replace(this.getStateHash(state, defaultState)); }

	static getDiffState (state, defaultState) {
		if (defaultState) {
			state = MiscUtil.cpy(state);
			Object.entries(defaultState).forEach(([k, v]) => {
				if (MiscUtil.eq(v, state[k])) delete state[k];
			});
		} else state = MiscUtil.cpy(state);

		return state;
	}

	/**
	 * @param state The state to save to the hash.
	 * @param [defaultState] Default state. If specified, the state will be filtered to non-default
	 *   values only.
	 */
	static getStateHash (state, defaultState) {
		state = this.getDiffState(state, defaultState);

		const stack = [];
		Object.entries(state).forEach(([k, v]) => stack.push(`${encodeURIComponent(k)}=${encodeURIComponent(JSON.stringify(v))}`));
		return `#${stack.join(UrlUtil.HASH_SEP)}`;
	}

	static _getHash () {
		return window.location.hash.slice(1).trim();
	}
}

UrlUtil.HASH_SEP = ",";

class DatetimeUtil {
	static getFormattedTimestamp (epoch) {
		const d = epoch ? new Date(epoch) : new Date();
		const monthName = DatetimeUtil._MONTHS[d.getMonth()];
		return `${d.getDate()} <span title="${monthName}">${monthName.substring(0, 3)}.</span> ${d.getFullYear()}, ${LogUtil._getTimestamp_getPadded(d.getHours())}:${LogUtil._getTimestamp_getPadded(d.getMinutes())}:${LogUtil._getTimestamp_getPadded(d.getSeconds())}`;
	}

	static getHumanReadableInterval (millis) {
		if (millis < 0 || isNaN(millis)) return "(Unknown interval)";

		const s = number => (number !== 1) ? "s" : "";

		const stack = [];

		let numSecs = Math.floor(millis / 1000);

		const numYears = Math.floor(numSecs / DatetimeUtil._SECS_PER_YEAR);
		if (numYears) {
			stack.push(`${numYears} year${s(numYears)}`);
			numSecs = numSecs - (numYears * DatetimeUtil._SECS_PER_YEAR);
		}

		const numDays = Math.floor(numSecs / DatetimeUtil._SECS_PER_DAY);
		if (numDays) {
			stack.push(`${numDays} day${s(numDays)}`);
			numSecs = numSecs - (numDays * DatetimeUtil._SECS_PER_DAY);
		}

		const numHours = Math.floor(numSecs / DatetimeUtil._SECS_PER_HOUR);
		if (numHours) {
			stack.push(`${numHours} hour${s(numHours)}`);
			numSecs = numSecs - (numHours * DatetimeUtil._SECS_PER_HOUR);
		}

		const numMinutes = Math.floor(numSecs / DatetimeUtil._SECS_PER_MINUTE);
		if (numMinutes) {
			stack.push(`${numMinutes} minute${s(numMinutes)}`);
			numSecs = numSecs - (numMinutes * DatetimeUtil._SECS_PER_MINUTE);
		}

		if (numSecs) {
			stack.push(`${numSecs} second${s(numSecs)}`);
		} else if (!stack.length) stack.push("less than a second"); // avoid adding this if there's already info

		return stack.join(", ");
	}
}

DatetimeUtil._MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
DatetimeUtil._SECS_PER_YEAR = 31536000;
DatetimeUtil._SECS_PER_DAY = 86400;
DatetimeUtil._SECS_PER_HOUR = 3600;
DatetimeUtil._SECS_PER_MINUTE = 60;

class ToastUtil {
	/**
	 * @param {Object|string} options
	 * @param {(jQuery|string)} options.content Toast contents. Supports jQuery objects.
	 * @param {string} options.type Toast type. Can be any of "success", "info", "warning", or "danger".
	 */
	static doToast (options) {
		if (typeof options === "string") {
			options = {
				content: options,
				type: "info",
			};
		}
		options.type = options.type || "info";

		const doCleanup = ($toast) => {
			$toast.addClass("toast--animate-exit");
			setTimeout(() => $toast.remove(), 85);
			ToastUtil._ACTIVE_TOAST.splice(ToastUtil._ACTIVE_TOAST.indexOf($toast), 1);
		};

		const $toast = $$`
		<div class="toast py-1 px-2 no-select toast--type-${options.type}">
			<div class="toast__wrp-content">${options.content}</div>
		</div>`
			.click(() => doCleanup($toast))
			.prependTo(document.body).data("pos", 0);

		setTimeout(() => $toast.addClass(`toast--animate-enter`), 5);
		setTimeout(() => doCleanup($toast), 5000);

		if (ToastUtil._ACTIVE_TOAST.length) {
			ToastUtil._ACTIVE_TOAST.forEach($oldToast => {
				const pos = $oldToast.data("pos");
				$oldToast.data("pos", pos + 1);
				if (pos === 2) doCleanup($oldToast);
			});
		}

		ToastUtil._ACTIVE_TOAST.push($toast);
	}
}

ToastUtil._ACTIVE_TOAST = [];

class PieUtil {
	static _getCoordinatesForPercent (percent) {
		const x = Math.cos(2 * Math.PI * percent);
		const y = Math.sin(2 * Math.PI * percent);
		return [x, y];
	}

	/**
	 * @param slices Array of objects of the form `{percent: 0.50, color: "#ff00ff"}`
	 * @param [opts] Options object.
	 * @param [opts.borderColor] Pie border colour.
	 * @param [opts.borderPercent] Border percentage width.
	 */
	static $getSvgPie (slices = [], opts = {}) {
		const $svg = $(`<svg viewBox="-1 -1 2 2" style="transform: rotate(-90deg)"></svg>`);

		let cumulativePercent = 0;
		slices.forEach(slice => {
			const [startX, startY] = this._getCoordinatesForPercent(cumulativePercent);
			cumulativePercent += slice.percent;
			const [endX, endY] = this._getCoordinatesForPercent(cumulativePercent);

			// if the slice is more than 50%, take the large arc (the long way around)
			const isLargeArc = slice.percent > 0.5 ? 1 : 0;

			const pathData = [
				`M ${startX} ${startY}`, // Move
				`A 1 1 0 ${isLargeArc} 1 ${endX} ${endY}`, // Arc
				`L 0 0`, // Line
			].join(" ");

			const elPath = document.createElementNS("http://www.w3.org/2000/svg", "path");
			elPath.setAttribute("d", pathData);
			elPath.setAttribute("fill", slice.color);
			$svg[0].appendChild(elPath);
		});

		if (opts.borderColor) {
			const elCircle = document.createElementNS("http://www.w3.org/2000/svg", "circle");
			const pctBorder = opts.borderPercent || 0.08;
			elCircle.setAttribute("cx", "0");
			elCircle.setAttribute("cy", "0");
			elCircle.setAttribute("r", `${1 - (pctBorder / 2)}`);
			elCircle.setAttribute("stroke", opts.borderColor);
			elCircle.setAttribute("fill", "none");
			elCircle.setAttribute("stroke-width", `${pctBorder}`);
			$svg[0].appendChild(elCircle);
		}

		return $svg;
	}

	static stringToColor (str) {
		let hash = 0;
		for (let i = 0; i < str.length; ++i) hash = str.charCodeAt(i) + ((hash << 5) - hash);

		let colour = "#";
		for (let i = 0; i < 3; ++i) {
			const value = (hash >> (i * 8)) & 0xFF;
			colour += value.toString(16).padStart(2, "0");
		}
		return colour;
	}
}

class CryptUtil {
	// region md5 internals
	// see: http://www.myersdaily.org/joseph/javascript/md5.js
	static _md5cycle (x, k) {
		let a = x[0]; let b = x[1]; let c = x[2]; let d = x[3];

		a = CryptUtil._ff(a, b, c, d, k[0], 7, -680876936);
		d = CryptUtil._ff(d, a, b, c, k[1], 12, -389564586);
		c = CryptUtil._ff(c, d, a, b, k[2], 17, 606105819);
		b = CryptUtil._ff(b, c, d, a, k[3], 22, -1044525330);
		a = CryptUtil._ff(a, b, c, d, k[4], 7, -176418897);
		d = CryptUtil._ff(d, a, b, c, k[5], 12, 1200080426);
		c = CryptUtil._ff(c, d, a, b, k[6], 17, -1473231341);
		b = CryptUtil._ff(b, c, d, a, k[7], 22, -45705983);
		a = CryptUtil._ff(a, b, c, d, k[8], 7, 1770035416);
		d = CryptUtil._ff(d, a, b, c, k[9], 12, -1958414417);
		c = CryptUtil._ff(c, d, a, b, k[10], 17, -42063);
		b = CryptUtil._ff(b, c, d, a, k[11], 22, -1990404162);
		a = CryptUtil._ff(a, b, c, d, k[12], 7, 1804603682);
		d = CryptUtil._ff(d, a, b, c, k[13], 12, -40341101);
		c = CryptUtil._ff(c, d, a, b, k[14], 17, -1502002290);
		b = CryptUtil._ff(b, c, d, a, k[15], 22, 1236535329);

		a = CryptUtil._gg(a, b, c, d, k[1], 5, -165796510);
		d = CryptUtil._gg(d, a, b, c, k[6], 9, -1069501632);
		c = CryptUtil._gg(c, d, a, b, k[11], 14, 643717713);
		b = CryptUtil._gg(b, c, d, a, k[0], 20, -373897302);
		a = CryptUtil._gg(a, b, c, d, k[5], 5, -701558691);
		d = CryptUtil._gg(d, a, b, c, k[10], 9, 38016083);
		c = CryptUtil._gg(c, d, a, b, k[15], 14, -660478335);
		b = CryptUtil._gg(b, c, d, a, k[4], 20, -405537848);
		a = CryptUtil._gg(a, b, c, d, k[9], 5, 568446438);
		d = CryptUtil._gg(d, a, b, c, k[14], 9, -1019803690);
		c = CryptUtil._gg(c, d, a, b, k[3], 14, -187363961);
		b = CryptUtil._gg(b, c, d, a, k[8], 20, 1163531501);
		a = CryptUtil._gg(a, b, c, d, k[13], 5, -1444681467);
		d = CryptUtil._gg(d, a, b, c, k[2], 9, -51403784);
		c = CryptUtil._gg(c, d, a, b, k[7], 14, 1735328473);
		b = CryptUtil._gg(b, c, d, a, k[12], 20, -1926607734);

		a = CryptUtil._hh(a, b, c, d, k[5], 4, -378558);
		d = CryptUtil._hh(d, a, b, c, k[8], 11, -2022574463);
		c = CryptUtil._hh(c, d, a, b, k[11], 16, 1839030562);
		b = CryptUtil._hh(b, c, d, a, k[14], 23, -35309556);
		a = CryptUtil._hh(a, b, c, d, k[1], 4, -1530992060);
		d = CryptUtil._hh(d, a, b, c, k[4], 11, 1272893353);
		c = CryptUtil._hh(c, d, a, b, k[7], 16, -155497632);
		b = CryptUtil._hh(b, c, d, a, k[10], 23, -1094730640);
		a = CryptUtil._hh(a, b, c, d, k[13], 4, 681279174);
		d = CryptUtil._hh(d, a, b, c, k[0], 11, -358537222);
		c = CryptUtil._hh(c, d, a, b, k[3], 16, -722521979);
		b = CryptUtil._hh(b, c, d, a, k[6], 23, 76029189);
		a = CryptUtil._hh(a, b, c, d, k[9], 4, -640364487);
		d = CryptUtil._hh(d, a, b, c, k[12], 11, -421815835);
		c = CryptUtil._hh(c, d, a, b, k[15], 16, 530742520);
		b = CryptUtil._hh(b, c, d, a, k[2], 23, -995338651);

		a = CryptUtil._ii(a, b, c, d, k[0], 6, -198630844);
		d = CryptUtil._ii(d, a, b, c, k[7], 10, 1126891415);
		c = CryptUtil._ii(c, d, a, b, k[14], 15, -1416354905);
		b = CryptUtil._ii(b, c, d, a, k[5], 21, -57434055);
		a = CryptUtil._ii(a, b, c, d, k[12], 6, 1700485571);
		d = CryptUtil._ii(d, a, b, c, k[3], 10, -1894986606);
		c = CryptUtil._ii(c, d, a, b, k[10], 15, -1051523);
		b = CryptUtil._ii(b, c, d, a, k[1], 21, -2054922799);
		a = CryptUtil._ii(a, b, c, d, k[8], 6, 1873313359);
		d = CryptUtil._ii(d, a, b, c, k[15], 10, -30611744);
		c = CryptUtil._ii(c, d, a, b, k[6], 15, -1560198380);
		b = CryptUtil._ii(b, c, d, a, k[13], 21, 1309151649);
		a = CryptUtil._ii(a, b, c, d, k[4], 6, -145523070);
		d = CryptUtil._ii(d, a, b, c, k[11], 10, -1120210379);
		c = CryptUtil._ii(c, d, a, b, k[2], 15, 718787259);
		b = CryptUtil._ii(b, c, d, a, k[9], 21, -343485551);

		x[0] = CryptUtil._add32(a, x[0]);
		x[1] = CryptUtil._add32(b, x[1]);
		x[2] = CryptUtil._add32(c, x[2]);
		x[3] = CryptUtil._add32(d, x[3]);
	}

	static _cmn (q, a, b, x, s, t) {
		a = CryptUtil._add32(CryptUtil._add32(a, q), CryptUtil._add32(x, t));
		return CryptUtil._add32((a << s) | (a >>> (32 - s)), b);
	}

	static _ff (a, b, c, d, x, s, t) { return CryptUtil._cmn((b & c) | ((~b) & d), a, b, x, s, t); }
	static _gg (a, b, c, d, x, s, t) { return CryptUtil._cmn((b & d) | (c & (~d)), a, b, x, s, t); }
	static _hh (a, b, c, d, x, s, t) { return CryptUtil._cmn(b ^ c ^ d, a, b, x, s, t); }
	static _ii (a, b, c, d, x, s, t) { return CryptUtil._cmn(c ^ (b | (~d)), a, b, x, s, t); }

	static _md51 (s) {
		let n = s.length;
		let state = [1732584193, -271733879, -1732584194, 271733878];
		let i;
		for (i = 64; i <= s.length; i += 64) {
			CryptUtil._md5cycle(state, CryptUtil._md5blk(s.substring(i - 64, i)));
		}
		s = s.substring(i - 64);
		let tail = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
		for (i = 0; i < s.length; i++) tail[i >> 2] |= s.charCodeAt(i) << ((i % 4) << 3);
		tail[i >> 2] |= 0x80 << ((i % 4) << 3);
		if (i > 55) {
			CryptUtil._md5cycle(state, tail);
			for (i = 0; i < 16; i++) tail[i] = 0;
		}
		tail[14] = n * 8;
		CryptUtil._md5cycle(state, tail);
		return state;
	}

	static _md5blk (s) {
		let md5blks = [];
		for (let i = 0; i < 64; i += 4) {
			md5blks[i >> 2] = s.charCodeAt(i) + (s.charCodeAt(i + 1) << 8) + (s.charCodeAt(i + 2) << 16) + (s.charCodeAt(i + 3) << 24);
		}
		return md5blks;
	}

	static _add32 (a, b) { return (a + b) & 0xFFFFFFFF; }

	static _rhex (n) {
		let s = "";
		for (let j = 0; j < 4; j++) {
			s += CryptUtil._hex_chr[(n >> (j * 8 + 4)) & 0x0F] + CryptUtil._hex_chr[(n >> (j * 8)) & 0x0F];
		}
		return s;
	}
	// endregion

	static hex (x) {
		for (let i = 0; i < x.length; i++) x[i] = CryptUtil._rhex(x[i]);
		return x.join("");
	}

	static hex2Dec (hex) {
		return parseInt(`0x${hex}`);
	}

	static md5 (s) {
		return CryptUtil.hex(CryptUtil._md51(s));
	}

	static uid () { // https://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
		return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c => (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16));
	}
}
CryptUtil._hex_chr = "0123456789abcdef".split("");

class ContextUtil {
	static _init () {
		if (ContextUtil._isInit) return;
		ContextUtil._isInit = true;

		$(document.body).click(() => ContextUtil._menus.forEach(menu => menu.close()));
	}

	static getMenu (actions) {
		ContextUtil._init();

		const menu = new ContextUtil.Menu(actions);
		ContextUtil._menus.push(menu);
		return menu;
	}

	static deleteMenu (menu) {
		menu.remove();
		const ix = ContextUtil._menus.findIndex(it => it === menu);
		if (~ix) ContextUtil._menus.splice(ix, 1);
	}

	static pOpenMenu (evt, menu, userData) {
		evt.preventDefault();
		evt.stopPropagation();

		ContextUtil._init();

		// Close any other open menus
		ContextUtil._menus.filter(it => it !== menu).forEach(it => it.close());

		return menu.pOpen(evt, userData);
	}
}
ContextUtil._isInit = false;
ContextUtil._menus = [];

ContextUtil.Menu = class {
	constructor (actions) {
		this._actions = actions;
		this._pResult = null;
		this._resolveResult = null;

		this._userData = null;

		const $elesAction = this._actions.map(it => {
			if (it == null) return $(`<div class="my-1 w-100 ui-ctx__divider"></div>`);

			const $row = $$`<div class="py-1 px-5 ui-ctx__row ${it.isDisabled ? "disabled" : ""} ${it.style || ""}">${it.text}</div>`
				.click(async evt => {
					if (it.isDisabled) return;

					evt.preventDefault();
					evt.stopPropagation();

					this.close();

					const result = await it.fnAction(evt, this._userData);
					if (this._resolveResult) this._resolveResult(result);
				});
			if (it.title) $row.title(it.title);

			return $row;
		});

		this._$ele = $$`<div class="flex-col ui-ctx__wrp py-2">${$elesAction}</div>`
			.hideX()
			.appendTo(document.body);
	}

	width () { return this._$ele.width(); }
	height () { return this._$ele.height(); }

	remove () { this._$ele.remove(); }

	pOpen (evt, userData) {
		if (this._resolveResult) this._resolveResult(null);
		this._pResult = new Promise(resolve => {
			this._resolveResult = resolve;
		});
		this._userData = userData;

		this._$ele
			.css({
				position: "absolute",
				left: this._getMenuPosition(evt, "x"),
				top: this._getMenuPosition(evt, "y"),
			})
			.showX();

		return this._pResult;
	}
	close () { this._$ele.hideX(); }

	_getMenuPosition (evt, axis) {
		const {fnMenuSize, propMousePos, fnWindowSize, fnScrollDir} = axis === "x"
			? {fnMenuSize: "width", propMousePos: "clientX", fnWindowSize: "width", fnScrollDir: "scrollLeft"}
			: {fnMenuSize: "height", propMousePos: "clientY", fnWindowSize: "height", fnScrollDir: "scrollTop"};

		const posMouse = evt[propMousePos];
		const szWin = $(window)[fnWindowSize]();
		const posScroll = $(window)[fnScrollDir]();
		let position = posMouse + posScroll;
		const szMenu = this[fnMenuSize]();
		// opening menu would pass the side of the page
		if (posMouse + szMenu > szWin && szMenu < posMouse) position -= szMenu;
		return position;
	}
}

/**
 * @param text
 * @param fnAction
 * @param [opts] Options object.
 * @param [opts.isDisabled] If this action is disabled.
 * @param [opts.title] Help text.
 * @param [opts.style] Additional CSS classes to add (e.g. `ctx-danger`).
 */
ContextUtil.Action = class {
	constructor (text, fnAction, opts) {
		opts = opts || {};

		this.text = text;
		this.fnAction = fnAction;

		this.isDisabled = opts.isDisabled;
		this.title = opts.title;
		this.style = opts.style;
	}
}

class JsonTree {
	/**
	 * @param opts
	 * @param opts.parentMeta
	 * @param opts.lastType
	 * @param opts.lastKey
	 * @param opts.obj
	 */
	static _getLeaf ({parentMeta, lastType, lastKey, obj} = {}) {
		const searchKeyKey = JSON.stringify(lastKey).toLowerCase();
		const searchKeyObj = JSON.stringify(obj).toLowerCase();

		const out = {
			fnSearch: ({term, isKeyOnly, isValOnly, isForceDisplay} = {}) => {
				const isVisible = isForceDisplay
					|| (!isValOnly && searchKeyKey.includes(term))
					|| (!isKeyOnly && searchKeyObj.includes(term));
				out.ele.classList.toggle("hidden", !isVisible);
				return Number(isVisible);
			},
		};

		const disp = document.createElement("div");
		disp.className = "inline-block cursor-copy px-1";
		disp.innerHTML = JSON.stringify(obj);
		disp.addEventListener("click", () => {
			ClipboardUtil.copyText(JSON.stringify(obj));
			ClipboardUtil.showCopiedEffect($(disp));
		});

		if (lastType === "object") {
			const dispLastKey = this._getDispKey({parentMeta, key: lastKey});
			out.ele = document.createElement("div");
			out.className = "ui-json__sect";
			const wrpDisp = document.createElement("code");
			wrpDisp.appendChild(dispLastKey);
			wrpDisp.append(" ");
			wrpDisp.appendChild(disp);
			out.ele.appendChild(wrpDisp);
		} else {
			out.ele = document.createElement("li");
			out.ele.className = "ui-json__li";
			const wrpDisp = document.createElement("code");
			wrpDisp.appendChild(disp);
			out.ele.appendChild(wrpDisp);
		}

		return out;
	}

	/**
	 * @param opts
	 * @param opts.parentMeta
	 * @param opts.key
	 */
	static _getDispKey ({parentMeta, key}) {
		const ele = document.createElement("b");
		ele.className = "cursor-alias";
		ele.innerHTML = `${key}:`;
		ele.addEventListener("click", () => parentMeta.doSetInputAndSearch(`k:${key.toLowerCase().trim()}`));
		return ele;
	}

	/**
	 * @param opts
	 * @param opts.parentMeta
	 * @param opts.obj
	 * @param opts.lastType
	 * @param opts.lastKey
	 * @param opts.wrp
	 * @param [opts.searchMeta]
	 */
	static _getWrpCollapsible ({parentMeta, obj, lastType, lastKey, wrp, searchMeta} = {}) {
		const btnCopy = document.createElement("button")
		btnCopy.className = `btn ${searchMeta ? "btn-xs" : "btn-xxs"} btn-default code mr-1`;
		btnCopy.setAttribute("title", "Copy");
		btnCopy.innerHTML = "C";
		btnCopy.addEventListener("click", () => {
			ClipboardUtil.copyText(JSON.stringify(obj));
			ClipboardUtil.showCopiedEffect($(btnCopy));
		});

		let isCollapsed = false;
		const btnCollapse = document.createElement("button");
		btnCollapse.className = `btn ${searchMeta ? "btn-xs" : "btn-xxs"} btn-default code`
		btnCollapse.addEventListener("click", () => {
			isCollapsed = !isCollapsed
			handleCollapse();
		});

		if (searchMeta) {
			searchMeta.wrpBtns.appendChild(btnCopy);
			searchMeta.wrpBtns.appendChild(btnCollapse);
		}

		const handleCollapse = () => {
			btnCollapse.innerHTML = isCollapsed ? "+" : "\u2012";
			btnCollapse.setAttribute("title", isCollapsed ? "Show Section" : "Hide Section");
			wrp.classList.toggle("hidden", isCollapsed);
		};
		handleCollapse();

		let eleHeader = searchMeta?.ele;
		if (!eleHeader) {
			eleHeader = document.createElement("code");
			eleHeader.className = "w-100 mr-2 inline-block";
			eleHeader.append(lastType === "array" || lastKey == null ? `` : this._getDispKey({parentMeta, key: lastKey}));
		}

		const out = document.createElement("div");
		out.className = "flex-col ui-json__sect";

		const el1 = document.createElement("div");
		el1.className = `w-100 flex-v-center ${lastType === "array" ? "ui-json__li" : ""}`;
		el1.appendChild(eleHeader);
		out.appendChild(el1);

		if (!searchMeta) {
			const el2 = document.createElement("div");
			el2.className = "flex-v-center ui-json__wrp-section-btns";
			el2.appendChild(btnCopy);
			el2.appendChild(btnCollapse);
			el1.appendChild(el2);
		}

		out.appendChild(wrp);

		return out;
	}

	/**
	 * @param opts
	 * @param opts.ele
	 * @param opts.lastKey
	 * @param opts.parentMeta
	 * @param opts.wrp
	 * @param [opts.searchMeta]
	 */
	static _getWrpMetaSearchable ({ele, lastKey, parentMeta, wrp, searchMeta} = {}) {
		const searchKey = `${lastKey}`.toLowerCase();
		const out = {
			ele,
			children: [],
			fnSearch: ({term, isKeyOnly, isValOnly, isForceDisplay} = {}) => {
				let cntVisible = Number(isForceDisplay || (!isValOnly && searchKey.includes(term)));
				isForceDisplay = isForceDisplay || !!cntVisible;
				out.children.forEach(it => cntVisible += it.fnSearch({term, isKeyOnly, isValOnly, isForceDisplay}));
				if (!searchMeta) wrp.classList.toggle("hidden", !(cntVisible > 0)); // Never hide the wrapper with the search bar
				return cntVisible;
			},
			doSetInputAndSearch: parentMeta.doSetInputAndSearch,
		};
		parentMeta.children.push(out);
		return out;
	}

	static _getBorderStyle (depth) {
		if (!depth) return "";
		return `border-left: ${depth ? JsonTree._INSET_LEVEL_PX : 0}px solid #eee;`;
	}

	/**
	 * @param opts
	 * @param opts.obj
	 * @param opts.parentMeta
	 * @param [opts.searchMeta]
	 * @param opts.lastType
	 * @param opts.lastKey
	 * @param opts.depth
	 */
	static _getArrayMeta ({obj, parentMeta, searchMeta, lastType, lastKey, depth} = {}) {
		const lst = document.createElement("ol");
		lst.className = "ui-json__lst my-0";
		lst.setAttribute("style", this._getBorderStyle(depth));

		const wrp = this._getWrpCollapsible({parentMeta, obj, lastType, lastKey, wrp: lst, searchMeta})
		parentMeta.ele.appendChild(wrp);

		return this._getWrpMetaSearchable({ele: lst, lastKey, parentMeta, wrp: wrp, searchMeta});
	}

	/**
	 * @param opts
	 * @param opts.obj
	 * @param opts.parentMeta
	 * @param [opts.searchMeta]
	 * @param opts.lastType
	 * @param opts.lastKey
	 * @param opts.depth
	 */
	static _getObjectMeta ({obj, parentMeta, searchMeta, lastType, lastKey, depth} = {}) {
		const wrpObject = document.createElement("div");
		wrpObject.className = "flex-col";
		wrpObject.setAttribute("style", this._getBorderStyle(depth));

		const wrp = this._getWrpCollapsible({parentMeta, obj, lastType, lastKey, wrp: wrpObject, searchMeta})
		parentMeta.ele.appendChild(wrp);

		return this._getWrpMetaSearchable({ele: wrpObject, lastKey, parentMeta, wrp, searchMeta});
	}

	/**
	 * opts Options object.
	 * opts.parentMeta
	 * [opts.searchMeta]
	 * opts.lastType
	 * opts.lastKey
	 * opts.obj
	 * opts.depth
	 */
	static _render_recurse ({parentMeta, searchMeta, lastType, obj, lastKey, depth} = {}) {
		if (obj == null) {
			const leaf = this._getLeaf({parentMeta, lastType, lastKey, obj})
			parentMeta.ele.appendChild(leaf.ele);
			parentMeta.children.push(leaf);
			return;
		}

		const to = typeof obj;
		switch (to) {
			case undefined:
			case "boolean":
			case "number":
			case "string": {
				const leaf = this._getLeaf({parentMeta, lastType, lastKey, obj})
				parentMeta.ele.appendChild(leaf.ele);
				parentMeta.children.push(leaf);
				return;
			}
			case "object": {
				if (obj instanceof Array) {
					const arrayMeta = this._getArrayMeta({obj, parentMeta, searchMeta, lastType, lastKey, depth});
					obj.forEach((nxt, i) => this._render_recurse({parentMeta: arrayMeta, lastType: "array", obj: nxt, lastKey: i, depth: depth + 1}));
				} else {
					const objectMeta = this._getObjectMeta({obj, parentMeta, searchMeta, lastType, lastKey, depth});
					Object.entries(obj).forEach(([k, nxt]) => this._render_recurse({parentMeta: objectMeta, lastType: "object", obj: nxt, lastKey: k, depth: depth + 1}));
				}
				return;
			}
			default: throw new Error(`Unhandled type "${to}"`);
		}
	}

	static render (json, $parent) {
		$parent.empty();

		const controlBar = new JsonTree._ControlBar();
		const {parentMeta, searchMeta} = controlBar.getRenderedMetas($parent[0]);

		this._render_recurse({
			parentMeta,
			searchMeta,
			lastType: json instanceof Array ? "array" : typeof json,
			obj: json,
			lastKey: null,
			depth: 0,
		});

		return {
			iptSearch: controlBar.iptSearch,
		};
	}
}
JsonTree._INSET_LEVEL_PX = 24;

JsonTree._ControlBar = class extends ComponentBase {
	constructor () {
		super();

		this._parentMeta = null;
		this._wrpHistory = null;
		this._iptSearch = null;
	}

	get iptSearch () { return this._iptSearch; }

	_renderLastSearches () {
		this._wrpHistory.innerHTML = "";

		this._state.lastSearches.forEach((it, i) => {
			const btn = document.createElement("button");
			btn.className = "mx-1 ui-json__btn-history btn btn-xxs btn-default";
			btn.innerHTML = `${it} <span class="muted">[${i + 1}]`;
			btn.title = `Hotkey: CTRL+${i + 1}`;
			btn.addEventListener("click", () => {
				this._doLoadHistory(i);
				this._iptSearch.focus();
			});
			this._wrpHistory.appendChild(btn);
		});
	}

	_doLoadHistory (n) {
		const term = this._state.lastSearches[n];
		if (!term) return;
		this._doSetInputAndSearch(term);
	}

	_doSetInputAndSearch (term) {
		this._state.lastVal = term;
		this._iptSearch.value = term || "";
		this._doSearch();
		this._pDoSaveSearch();
	}

	_doSearch () {
		let term = this._state.lastVal;
		const mod = term.startsWith("k:") ? "isKeyOnly" : term.startsWith("v:") ? "isValOnly" : null;
		if (mod) term = term.split(":").slice(1).join(":");
		const searchMeta = {term};
		if (mod) searchMeta[mod] = true;
		this._parentMeta.children.forEach(it => it.fnSearch(searchMeta));
	}

	_pDoSaveSearch () {
		if (!this._state.lastVal) return;
		if (!this._state.lastSearches) return;
		if (this._state.lastSearches[0] === this._state.lastVal) return;

		// If the term exists, "move" it to the front of the list
		const ixExisting = this._state.lastSearches.indexOf(this._state.lastVal);
		if (~ixExisting) this._state.lastSearches.splice(ixExisting, 1);

		this._state.lastSearches.unshift(this._state.lastVal);
		this._state.lastSearches = this._state.lastSearches.slice(0, 5);

		StorageUtil.pSet(JsonTree._ControlBar._STORAGE_KEY, this._state.lastSearches);

		this._renderLastSearches();
	}

	getRenderedMetas (parent) {
		this._parentMeta = {ele: parent, children: []};

		const btnShowHelp = this._getBtnHelp();
		const wrpBtns = document.createElement("div");
		wrpBtns.className = "flex-v-center";
		wrpBtns.appendChild(btnShowHelp);

		this._wrpHistory = document.createElement("div");
		this._wrpHistory.className = "flex-v-center overflow-x-auto mb-1 py-1 ui-json__wrp-history";

		StorageUtil.pGet(JsonTree._ControlBar._STORAGE_KEY)
			.then(it => {
				this._state.lastSearches = it || [];
				this._renderLastSearches();
			});

		this._parentMeta.doSetInputAndSearch = this._doSetInputAndSearch.bind(this);

		this._iptSearch = document.createElement("input");
		this._iptSearch.className = "w-100 form-control mr-1";
		this._iptSearch.placeholder = "Search...";

		const keyupDebounced1 = FnUtil.debounce(() => this._doSearch(), 225);
		const keyupDebounced2 = FnUtil.debounce(() => this._pDoSaveSearch(), 1000);
		this._iptSearch.addEventListener("keyup", evt => {
			this._state.lastVal = (this._iptSearch.value || "").trim().toLowerCase();
			if (evt.key === "Enter") this._pDoSaveSearch();

			keyupDebounced1();
			keyupDebounced2();
		});
		this._iptSearch.addEventListener("keydown", evt => {
			if (evt.key === "Escape") {
				evt.stopPropagation();
				this._iptSearch.blur();
				return;
			}

			if (evt.ctrlKey) {
				switch (evt.key) {
					case "1":
					case "2":
					case "3":
					case "4":
					case "5": {
						if (!this._state.lastSearches) return;

						evt.preventDefault();
						evt.stopPropagation();

						const n = Number(evt.key) - 1;
						this._doLoadHistory(n);
					}
				}
			}
		});

		const wrpSearch = document.createElement("div");
		wrpSearch.className = "flex-col w-100";

		const el1 = document.createElement("div");
		wrpSearch.appendChild(el1);

		el1.className = "flex-v-center"
		el1.appendChild(this._iptSearch);
		el1.appendChild(wrpBtns);

		wrpSearch.appendChild(this._wrpHistory);

		const searchMeta = {
			ele: wrpSearch,
			wrpBtns: wrpBtns,
		};

		return {
			searchMeta,
			parentMeta: this._parentMeta,
		};
	}

	_getBtnHelp () {
		const btnHelp = document.createElement("button");
		btnHelp.className = "btn btn-xs btn-default code mr-1";
		btnHelp.title = "Help";
		btnHelp.innerHTML = "?";
		btnHelp.addEventListener("click", () => {
			const {$wrpInner, doClose} = UiUtil.getShowModal({isMaxWidth600: true, isFullWidth: true});

			const $btnClose = $(`<button class="btn btn-default btn-sm">OK</button>`)
				.click(() => doClose());

			$$($wrpInner)`<h3 class="my-2">Help</h3>
				<p class="mt-1"><b>Search</b></p>
				<p class="ml-1"><code>k:&lt;keyName&gt;</code> \u2014 Search for a key (a key is displayed in bold)</p>
				<p class="ml-1"><code>v:&lt;value&gt;</code> \u2014 Search for a value (a value is displayed as plain text)</p>
				<p class="mt-2"><b>History</b></p>
				<p class="ml-1">The last five searches are saved. Click one, or press <key>CTRL+&lt;n&gt;</key>, to retrieve it.</p>
				<div class="flex-h-right mt-2">${$btnClose}</div>`;
		});

		return btnHelp;
	}

	_getDefaultState () {
		return {
			lastVal: null,
			lastSearches: null,
		};
	}
}
JsonTree._ControlBar._STORAGE_KEY = "JsonTree_lastSearches";

class StorageUtil {
	// region Synchronous
	static getSyncStorage () {
		if (StorageUtil._init) {
			if (StorageUtil.__fakeStorage) return StorageUtil._fakeStorage;
			else return window.localStorage;
		}
		StorageUtil._init = true;

		try {
			window.localStorage.setItem("_test_storage", true);
			return window.localStorage;
		} catch (e) {
			// if the user has disabled cookies, build a fake version
			StorageUtil.__fakeStorage = true;
			StorageUtil._fakeStorage = {
				isSyncFake: true,
				getItem: k => StorageUtil.__fakeStorage[k],
				removeItem: k => delete StorageUtil.__fakeStorage[k],
				setItem: (k, v) => StorageUtil.__fakeStorage[k] = v,
			};
			return StorageUtil._fakeStorage;
		}
	}

	static syncGet (key) {
		const rawOut = StorageUtil.getSyncStorage().getItem(key);
		if (rawOut && rawOut !== "undefined" && rawOut !== "null") return JSON.parse(rawOut);
		return null;
	}

	static syncSet (key, value) {
		StorageUtil.getSyncStorage().setItem(key, JSON.stringify(value));
		StorageUtil._syncTrackKey(key)
	}

	static syncRemove (key) {
		StorageUtil.getSyncStorage().removeItem(key);
		StorageUtil._syncTrackKey(key, true);
	}

	static _syncTrackKey (key, isRemove) {
		const meta = StorageUtil.syncGet(StorageUtil._META_KEY) || {};
		if (isRemove) delete meta[key];
		else meta[key] = 1;
		StorageUtil.getSyncStorage().setItem(StorageUtil._META_KEY, JSON.stringify(meta));
	}

	static syncGetDump () {
		const out = {};
		const meta = StorageUtil.syncGet(StorageUtil._META_KEY) || {};
		Object.entries(meta).filter(([key, isPresent]) => isPresent).forEach(([key]) => out[key] = StorageUtil.syncGet(key));
		return out;
	}

	static syncSetFromDump (dump) {
		Object.entries(dump).forEach(([k, v]) => StorageUtil.syncSet(k, v));
	}
	// endregion

	// region Asynchronous
	static async getAsyncStorage () {
		if (StorageUtil._initAsync) {
			if (StorageUtil.__fakeStorageAsync) return StorageUtil._fakeStorageAsync;
			else return localforage;
		}

		const getInitFakeStorage = () => {
			StorageUtil.__fakeStorageAsync = {};
			StorageUtil._fakeStorageAsync = {
				pIsAsyncFake: true,
				async setItem (k, v) { StorageUtil.__fakeStorageAsync[k] = v; },
				async getItem (k) { return StorageUtil.__fakeStorageAsync[k]; },
				async removeItem (k) { delete StorageUtil.__fakeStorageAsync[k]; },
			};
			return StorageUtil._fakeStorageAsync;
		};

		if (typeof window !== "undefined" && typeof localforage !== "undefined") {
			try {
				// check if IndexedDB is available (i.e. not in Firefox private browsing)
				await new Promise((resolve, reject) => {
					const request = window.indexedDB.open("_test_db", 1);
					request.onerror = reject;
					request.onsuccess = resolve;
				});
				await localforage.setItem("_storage_check", true);
				return localforage;
			} catch (e) {
				return getInitFakeStorage();
			} finally {
				StorageUtil._initAsync = true;
			}
		} else return getInitFakeStorage();
	}

	static async pSet (key, value) {
		StorageUtil._pTrackKey(key);
		const storage = await StorageUtil.getAsyncStorage();
		return storage.setItem(key, value);
	}

	static async pGet (key) {
		const storage = await StorageUtil.getAsyncStorage();
		return storage.getItem(key);
	}

	static async pRemove (key) {
		StorageUtil._pTrackKey(key, true);
		const storage = await StorageUtil.getAsyncStorage();
		return storage.removeItem(key);
	}

	static async _pTrackKey (key, isRemove) {
		const storage = await StorageUtil.getAsyncStorage();
		const meta = (await StorageUtil.pGet(StorageUtil._META_KEY)) || {};
		if (isRemove) delete meta[key];
		else meta[key] = 1;
		return storage.setItem(StorageUtil._META_KEY, meta);
	}

	static async pGetDump () {
		const out = {};
		const meta = (await StorageUtil.pGet(StorageUtil._META_KEY)) || {};
		await Promise.all(Object.entries(meta).filter(([key, isPresent]) => isPresent).map(async ([key]) => out[key] = await StorageUtil.pGet(key)));
		return out;
	}

	static async pSetFromDump (dump) {
		return Promise.all(Object.entries(dump).map(([k, v]) => StorageUtil.pSet(k, v)));
	}
	// endregion
}
StorageUtil._init = false;
StorageUtil._initAsync = false;
StorageUtil._fakeStorage = {};
StorageUtil._fakeStorageAsync = {};
StorageUtil._META_KEY = "_STORAGE_META_STORAGE";

class ClipboardUtil {
	static showCopiedEffect ($ele, text = "Copied!") {
		const top = $(window).scrollTop();
		const pos = $ele.offset();

		const $dispCopied = $(`<div class="clp__disp-copied px-2"></div>`)
			.html(text)
			.css({
				top: (pos.top - 24) - top,
				left: pos.left + ($ele.width() / 2),
			})
			.appendTo(document.body)
			.animate(
				{
					top: "-=8",
					opacity: 0,
				},
				250,
				() => $dispCopied.remove(),
			);
	}

	static copyText (text) {
		const $iptTemp = $(`<textarea class="clp__wrp-temp"></textarea>`)
			.appendTo(document.body)
			.val(text)
			.select();
		document.execCommand("Copy");
		$iptTemp.remove();
	}
}

class EventUtil {
	static isInInput (evt) {
		return evt.target.nodeName === "INPUT" || evt.target.nodeName === "TEXTAREA"
			|| evt.target.getAttribute("contenteditable") === "true";
	}
}

class DownloadUtil {
	static doDownloadJson (filename, data) {
		if (typeof data !== "string") data = JSON.stringify(data, null, "\t");
		return DownloadUtil._doDownload(filename, data, "text/json");
	}

	static doDownloadText (filename, string) {
		return DownloadUtil._doDownload(filename, string, "text/plain");
	}

	static _doDownload (filename, data, mimeType) {
		const a = document.createElement("a");
		const t = new Blob([data], {type: mimeType});
		a.href = URL.createObjectURL(t);
		a.download = filename;
		a.target = "_blank";
		a.style.display = "none";
		document.body.appendChild(a);
		a.click();
		document.body.removeChild(a);
	}
}

class InputUiUtil {
	/**
	 * @param [opts] Options.
	 * @param [opts.title] Prompt title.
	 * @param [opts.textYesRemember] Text for "yes, and remember" button.
	 * @param [opts.textYes] Text for "yes" button.
	 * @param [opts.textNo] Text for "no" button.
	 * @param [opts.btnYesClass] Additional class names for the "yes" button.
	 * @param [opts.htmlDescription] Description HTML for the modal.
	 * @param [opts.storageKey] Storage key to use when "remember" options are passed.
	 * @param [opts.fnRemember] Custom function to run when saving the "yes and remember" option.
	 * @param [opts.isSkippable] If the prompt is skippable.
	 * @return {Promise} A promise which resolves to true/false if the user chose, or null otherwise.
	 */
	static async pGetUserBoolean (opts) {
		opts = opts || {};

		if (opts.storageKey) {
			const prev = await StorageUtil.pGet(opts.storageKey);
			if (prev != null) return prev;
		}

		return new Promise(resolve => {
			const $btnTrueRemember = opts.textYesRemember ? $(`<button class="btn btn-primary flex-v-center mr-2">${opts.textYesRemember}</button>`)
				.click(() => {
					doClose(true, true);
					if (opts.fnRemember) opts.fnRemember(true);
					else StorageUtil.pSet(opts.storageKey, true);
				}) : null;

			const $btnTrue = $(`<button class="btn btn-primary flex-v-center mr-3 ${opts.btnYesClass || ""}">${opts.textYes || "OK"}</button>`)
				.click(() => doClose(true));

			const $btnFalse = $(`<button class="btn btn-default btn-sm flex-v-center">${opts.textNo || "Cancel"}</button>`)
				.click(() => doClose(false));

			const $btnSkip = !opts.isSkippable ? null : $(`<button class="btn btn-default ml-3">Skip</button>`)
				.click(() => doClose(InputUiUtil.SYM_UI_SKIP));

			const {$wrpInner, doClose} = UiUtil.getShowModal({
				fnClose: (response) => {
					resolve(response);
				},
			});

			if (opts.title && opts.title.trim()) $$`<h4 class="my-1">${opts.title}</h4>`.appendTo($wrpInner)
			if (opts.htmlDescription && opts.htmlDescription.trim()) $$`<div class="flex w-100 mb-1">${opts.htmlDescription}</div>`.appendTo($wrpInner);
			$$`<div class="flex-v-center flex-h-right py-1 px-1">${$btnTrueRemember}${$btnTrue}${$btnFalse}${$btnSkip}</div>`.appendTo($wrpInner);
			$btnTrue.focus();
			$btnTrue.select();
		});
	}
}
InputUiUtil.SYM_UI_SKIP = Symbol("uiSkip");

export {
	CommonConst,
	DataUtil,
	JqueryUtil,
	UiUtil,
	ComponentUtil,
	ProxyBase,
	ComponentBase,
	HashStateComponentBase,
	FnUtil,
	UrlUtil,
	DatetimeUtil,
	ToastUtil,
	PieUtil,
	CryptUtil,
	ContextUtil,
	JsonTree,
	StorageUtil,
	ClipboardUtil,
	EventUtil,
	DownloadUtil,
	InputUiUtil,
};
